<?php

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Инициализируем переменные для подключения к базе данных.
$db_user = 'u20495';   // Логин БД
$db_pass = '1204499';  // Пароль БД

// Подключаемся к базе данных на сервере.
$db = new PDO('mysql:host=localhost;dbname=u20495', $db_user, $db_pass, array(
  PDO::ATTR_PERSISTENT => true
));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$admin_check=0;
if(!empty($_SERVER['PHP_AUTH_USER'])&&!empty($_SERVER['PHP_AUTH_PW'])){
$request = 'SELECT * from Admin';
$result = $db->prepare($request);
$result->execute();
while ($data = $result->fetch()) {
    if ($data['login'] == $_SERVER['PHP_AUTH_USER'] && $data['pasword']==md5($_SERVER['PHP_AUTH_PW'])) {
        $admin_check = 1;
    }
}}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();
  $messages['save'] = '';     // Сообщение об успешном отправлении данных
  $messages['notsave'] = '';  // Сообщение об ошибке отправления данных
  $messages['name'] = '';     // Сообщение об ошибке в имени
  $messages['email'] = '';    // Сообщение об ошибке в email
  $messages['powers'] = '';   // Сообщение об ошибке в способностях
  $messages['biography'] = '';      // Сообщение об ошибке в биографии
  $messages['know_check'] = '';    // Сообщение об ошибке в согласии с контрактом

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  // Функция empty возвращает true, если кука пустая.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куки, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);
    // Выводим сообщение пользователю.
    $messages['save'] = 'Результаты отправлены';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['pass'])) {
      $messages['savelogin'] = sprintf(' <a href="login.php">Войти для обновления данных</a>
       Логин: <strong>%s</strong>
       Паролем: <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }else{
    $messages['savelogin']='<a href="login.php">Войти</a> как пользователь  если вы ранее отпраляли форму';
  }

  // Выдаем сообщение об ошибке сохранения.
  if (!empty($_COOKIE['notsave'])) {
    // Удаляем куки, указывая время устаревания в прошлом.
    setcookie('notsave', '', 100000);
    $messages['notsave'] = strip_tags($_COOKIE['notsave']);
  }

  // Складываем признаки ошибок в массив.
  $errors = array();
  $errors['name'] = !empty($_COOKIE['name_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['powers'] = !empty($_COOKIE['powers_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['know_check'] = !empty($_COOKIE['know_check_error']);
  $errors['date1'] = !empty($_COOKIE['date1_error']);
  $errors['member_num'] = !empty($_COOKIE['member_num_error']);
  $errors['gender'] = !empty($_COOKIE['gender_error']);

  // Проверка на ошибки в имени.
  if ($errors['name'] == 'null') {
    setcookie('name_error', '', 100000);
    $messages['name'] = '<div class="error">Заполните имя.</div>';
  }
  else if ($errors['name'] == 'incorrect') {
      setcookie('name_error', '', 100000);
      $messages['name'] = '<div class="error">Недопустимые символы. Введите имя заново.</div>';
  }

  // Проверка ошибок в email.
  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages['email'] = '<div class="error">Заполните поле email.</div>';
  }

  // Проверка ошибок в способностях.
  if ($errors['powers']) {
    setcookie('powers_error', '', 100000);
    $messages['powers'] = '<div class="error">Выберите хотя бы одну сверхспособность.</div>';
  }

  // Проверка ошибок с биографии.
  if ($errors['biography']) {
    setcookie('biography_error', '', 100000);
    $messages['biography'] = '<div class="error">Заполните поле биография.</div>';
  }

  // Проверка ошибок согласия с контрактом.
  if ($errors['know_check']) {
    setcookie('know_check_error', '', 100000);
    $messages['know_check'] = '<div class="error">Потвердите ознакомление с контрактом.</div>';
  }

  // Создаем массив для способностей.
  $powers = array();

  // Заполняем массив способностей. Ключ - будет отправляться в базу данных,
  // а значение по этому ключу будет отображаться в форме.
  $powers['imm'] = "Бессмертие";
  $powers['troughtw'] = "Прохождение сквозь стены";
  $powers['levit'] = "Левитация";

  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();

  // Для этого используем тернарный оператор. Если кука пустая, присваиеваем
  // значение по умолчанию или пустую строку. Иначе присваем значение этой куки.
  $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date1'] = empty($_COOKIE['year_value']) ? '' : strip_tags($_COOKIE['year_value']);
  $values['gender'] = empty($_COOKIE['gender_value']) ? 'W' : strip_tags($_COOKIE['gender_value']);
  $values['member_num'] = empty($_COOKIE['member_num_value']) ? '4' : strip_tags($_COOKIE['member_num_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  $powers_value = empty($_COOKIE['powers_value']) ? '' : json_decode($_COOKIE['powers_value']);

  // Для способностей инициализация происходит немного по-другому.
  // Необходимо в $values по ключу powers создать массив и заполнить его
  // значениями из массива $powers.
  $values['powers'] = [];

  // Проверяем является ли $powers_value массивом и он не равен null.
  if (isset($powers_value) && is_array($powers_value)) {
    foreach ($powers_value as $power) {
      if (!empty($powers[$power])) {
        $values['powers'][$power] = $power;
      }
    }
  }
  //Проверяем отправлен ли запрос админом на изменение данных
  if (!empty($_SERVER['PHP_AUTH_USER']) &&
  !empty($_SERVER['PHP_AUTH_PW']) &&
  $admin_check &&
  !empty($_GET['user'])) {

    $messages['save'] = ' ';
    $messages['savelogin'] = 'Изменение данных для пользывателя с логином:'.$_GET['user'];

    // Пытаемся достать данные пользователя из БД.
    try {
      // Подготавливаем SQL запрос.
      $stmt = $db->prepare("SELECT * FROM pr5F WHERE login = ?");
      // Выполняем запрос.
      $stmt->execute(array(
        $_GET['user']
      ));
      // Получаем данные в виде массива из БД.
      $user_data = $stmt->fetch();

      // Инициализируем $values значениями из массива полученного из БД
      // предварительно их санитизовав.
      $values['name'] = strip_tags($user_data['name']);
      $values['email'] = strip_tags($user_data['email']);
      $values['date1'] = strip_tags($user_data['Date']);
      $values['gender'] = strip_tags($user_data['sex']);
      $values['member_num'] = strip_tags($user_data['limbs']);
      $values['biography'] = strip_tags($user_data['bio']);
      $powers_value = explode(", ", $user_data['powers']);

      // Как и в предыдущем разе заполняем массив способностей.
      $values['powers'] = [];
      foreach ($powers_value as $power) {
        if (!empty($powers[$power])) {
          $values['powers'][$power] = $power;
        }
      }

    } catch(PDOException $e) {
      // При возникновении ошибки получения данных из БД, выводим информацию
      // об ошибке пользователю.
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      // Прекращаем работу скрипта
      exit();
    }
  }
  else
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {

    $messages['save'] = ' ';
    $messages['savelogin'] = 'Вход выполнен.После отправки ваши данные будут обновлены.Ваш логин:'.$_SESSION['login'];

    // Пытаемся достать данные пользователя из БД.
    try {
      // Подготавливаем SQL запрос.
      $stmt = $db->prepare("SELECT * FROM pr5F WHERE login = ?");
      // Выполняем запрос.
      $stmt->execute(array(
        $_SESSION['login']
      ));
      // Получаем данные в виде массива из БД.
      $user_data = $stmt->fetch();

      // Инициализируем $values значениями из массива полученного из БД
      // предварительно их санитизовав.
      $values['name'] = strip_tags($user_data['name']);
      $values['email'] = strip_tags($user_data['email']);
      $values['date1'] = strip_tags($user_data['Date']);
      $values['gender'] = strip_tags($user_data['sex']);
      $values['member_num'] = strip_tags($user_data['limbs']);
      $values['biography'] = strip_tags($user_data['bio']);
      $powers_value = explode(", ", $user_data['powers']);

      // Как и в предыдущем разе заполняем массив способностей.
      $values['powers'] = [];
      foreach ($powers_value as $power) {
        if (!empty($powers[$power])) {
          $values['powers'][$power] = $power;
        }
      }

    } catch(PDOException $e) {
      // При возникновении ошибки получения данных из БД, выводим информацию
      // об ошибке пользователю.
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      // Прекращаем работу скрипта
      exit();
    }
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить
// в базу данных.
else {
  //If admin request deletion
  if(!empty($_SERVER['PHP_AUTH_USER']) &&
  !empty($_SERVER['PHP_AUTH_PW']) &&
  $admin_check &&
  !empty($_POST['delete'])){
    try {
      $stmt = $db->prepare('DELETE FROM pr5U WHERE login = ?');
      $stmt->execute(array(
          $_POST['delete']
      ));
      $stmt = $db->prepare('DELETE FROM pr5F WHERE login = ?');
      $stmt->execute(array(
          $_POST['delete']
      ));
  } catch (PDOException $e) {
      echo 'Ошибка: ' . $e->getMessage();
      exit();
  }
  header('Location: admin.php');
  exit();
  }
  // Проверяем ошибки.
  // Для этого создаем логическую переменную и по умолчанию устанавливаем false.
  $errors = FALSE;
  if (empty($_POST['name'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('name_error', 'null', time() + 24 * 60 * 60);
    // Помечаем наличие ошибки.
    $errors = TRUE;
  }
  else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["name"])) {
      setcookie('name_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
  }

  // Аналогично проверяем ошибки для всех остальных полей.
  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле name.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  // Для способностей опять другой метод проверки на ошибки.
  // Созадем массив способностей.
  $powers = array();

  // Заполняем его значениями из глобальной переменной $_POST.
  foreach ($_POST['powers'] as $key => $value) {
      $powers[$key] = $value;
  }

  // Если размер массива нулевой, значит пользователь не выбрал ни одной
  // способности. Соответственно, обрабатываем эту ошибку.
  if (!sizeof($powers)) {
    setcookie('powers_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('powers_value', json_encode($powers), time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['biography'])) {
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['know_check'])) {
    
    setcookie('know_check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  // Для значений, где ошибок не должно быть, т.к. там есть значения по
  // умолчанию просто создаем куки и заполняем их.
  setcookie('year_value', $_POST['date1'], time() + 30 * 24 * 60 * 60);
  setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
  setcookie('member_num_value', $_POST['member_num'], time() + 30 * 24 * 60 * 60);

  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
   
    header('Location: index.php');
    exit();
  }
  else {
    // Иначе удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('powers_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('know_check_error', '', 100000);
  }

  if (!empty($_SERVER['PHP_AUTH_USER']) &&
  !empty($_SERVER['PHP_AUTH_PW']) &&
  $admin_check&&
  !empty($_POST['edit'])) {
      try {
      // Подготавливаем SQL запрос.
      $stmt = $db->prepare("UPDATE pr5F SET name = ?, email = ?, Date = ?, sex = ?, limbs = ?, powers = ?, bio = ? WHERE login = ?");
      // // Выполняем запрос.
      $stmt->execute(array(
        $_POST['name'],
        $_POST['email'],
        $_POST['date1'],
        $_POST['gender'],
        $_POST['member_num'],
        implode(', ', $_POST['powers']),
        $_POST['biography'],
        $_POST['edit']
      ));
    } catch(PDOException $e) {
      // При возникновении ошибки получения данных из БД, выводим информацию
      // об ошибке пользователю.
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      // Прекращаем работу скрипта.
      exit();
    }
    header('Location: admin.php');
    exit();
    }

  else
  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {

    // Пытаемся отправить новые данные пользователя в базу данных.
    try {
      // Подготавливаем SQL запрос.
      $stmt = $db->prepare("UPDATE pr5F SET name = ?, email = ?, Date = ?, sex = ?, limbs = ?, powers = ?, bio = ? WHERE login = ?");
      // // Выполняем запрос.
      $stmt->execute(array(
        $_POST['name'],
        $_POST['email'],
        $_POST['date1'],
        $_POST['gender'],
        $_POST['member_num'],
        implode(', ', $_POST['powers']),
        $_POST['biography'],
        $_SESSION['login']
      ));
    } catch(PDOException $e) {
      // При возникновении ошибки получения данных из БД, выводим информацию
      // об ошибке пользователю.
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      // Прекращаем работу скрипта.
      exit();
    }

  }
  else {
    // Иначе, если отправляются новые данные.
    // Генерируем уникальный логин и пароль.
    $login = uniqid("User");
    $pass = rand(123456, 999999);
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('pass', $pass);

    // Пытаемся отправить данные в базу данных.
    try {
      // Подготавливаем SQL запрос.
      $stmt_form = $db->prepare("INSERT INTO pr5F  SET login = ?, name = ?, email = ?, Date = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
      // // Выполняем запрос.
      $stmt_form->execute(array(
        $login,
        $_POST['name'],
        $_POST['email'],
        $_POST['date1'],
        $_POST['gender'],
        $_POST['member_num'],
        implode(', ', $_POST['powers']),
        $_POST['biography']
      ));
      // Подготавливаем еще один SQL запрос, где вопросы заменятся на значения.
      // Запрос направляется в таблицу users с данными пользователей.
      $stmt_user = $db->prepare("INSERT INTO pr5U SET login = ?, pass = ?");
      $stmt_user->execute(array(
        $login,
        // В базу отправляем пароль шифруя его с помощью алгоритма sha256.
        hash('sha256', $pass, false)
      ));
    } catch(PDOException $e) {
      // При возникновении ошибки получения данных из БД, выводим информацию
      // об ошибке пользователю.
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      // Прекращаем работу скрипта.
      exit();
    }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}