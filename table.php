<table>
    <thead>
        <tr>
            <th colspan="7">Текущие данные</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Имя</td>
            <td>Email</td>
            <td>Дата рождения</td>
            <td>Пол</td>
            <td>Кол-во конечностей</td>
            <td>Биография</td>
            <td>Суперсилы</td>
        </tr>
        <tr> 
        <td><?php print $values['name']?></td>
        <td><?php print$values['email']?></td> 
        <td><?php print$values['date1']?></td> 
        <td><?php print$values['gender']?></td> 
        <td><?php print$values['member_num']?></td> 
        <td><?php print$values['biography']?></td> 
        <td><?php   
            foreach ($powers as $key => $value) {
              $selected = empty($values['powers'][$key]) ? '' : ' selected="selected" ';
              if($selected)
              printf('%s ',  $value);
            }
            ?></td>
        </tr>
    </tbody>
</table>